﻿using System;
using System.IO;

public class Program
{

    private int field = 1991;
    public static void Main()
    {
        while (true)
        {
            Console.WriteLine("1. Count words in Lorem Ipsum");
            Console.WriteLine("2. Perform a sum operation");
            Console.WriteLine("3. Exit");
            Console.Write("Enter your choice: ");
            var choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    GetWords();
                    break;
                case "2":
                    PerformSumOperation();
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Invalid.");
                    break;
            }
        }
    }

    static void GetWords()
    {
        Console.Write("Enter number of words to display: ");
        int numWords = int.Parse(Console.ReadLine());

        string text = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "lorem.txt"));
        string[] words = text.Split(' ');

        for (int i = 0; i < numWords && i < words.Length; i++)
        {
            Console.WriteLine(words[i]);
        }
    }

    static void PerformSumOperation()
    {
        Console.Write("Enter a mathematical first operand: ");
        int firstOperand = int.Parse(Console.ReadLine());

        Console.Write("Enter a mathematical second operand: ");
        int secondOperand = int.Parse(Console.ReadLine());

        double result = firstOperand + secondOperand;

        Console.WriteLine("Result: " + result);
    }
}